#!/usr/bin/env python3

import sys

def dict_convert(word1, word2):

    match_dict = dict()
    min_word = ""

    if (len(word1) > len(word2)):
        min_word = word2
        for c in word1:
            if c not in match_dict.keys():
                match_dict[c] = 1
            else:
                match_dict[c] += 1
    else:
        min_word = word1
        for c in word2:
            if c not in match_dict.keys():
                match_dict[c] = 1
            else:
                match_dict[c] += 1

    return match_dict, min_word

def find_perm(match_dict, word):

    matches = []

    for c in word:
        if c in match_dict.keys():
            matches.append(c)

    return ''.join(sorted(matches))


if __name__ == '__main__':

    for line in sys.stdin:
        word1 = line.strip()
        word2 = sys.stdin.readline().strip()

        match_dict, word = dict_convert(word1, word2)

        perm = find_perm(match_dict, word)
        print(perm)
